#include <iostream>
#include <ctime>

using namespace std;

class Person
{
private:
    std::string name;
    std::string jobTitle;

public:
    Person(std::string name, std::string jobTitle) : name(name), jobTitle(jobTitle){
    }

    std::string getName(){
        return name;
    }

    std::string getJobTitle(){
        return jobTitle;
    }
};

class Worker : public Person
{
private:
    char task = 0;

public:
    Worker(std::string name) : Person(name, "Worker"){

    }

    bool isBusy(){
        return task != 0;
    }

    void setTask(char task){
        this->task = task;
    }

    char getTask(){
        return task;
    }
};

class Manager : public Person
{
private:
    const char workType[3] = {'A', 'B', 'C'};
    Worker** worker;
    int teamSize;
    int teamNumber;
public:
    Manager(std::string name, int teamNumber, int numWorkers) : Person(name, "Manager"), teamNumber(teamNumber){
        teamSize = numWorkers;
        worker = new Worker* [numWorkers];
        for(int i = 0; i < numWorkers; i++){
            worker[i] = new Worker("Worker " + to_string(i));
        }
    }

    ~Manager(){
        for(int i = 0; i < teamSize; i++)
            delete[] worker[i];
        delete[] worker;
    }

    bool isBusy(){
        bool busy = true;
        for(int i = 0; i < teamSize; i++){
            if(worker[i]->isBusy() == false){
                busy = false;
                break;
            }
        }
        return busy;
    }

    int getTeamNumber(){
        return teamNumber;
    }

    void setTask(int task){
        if(isBusy())
            return;
        std::srand(teamNumber + task);
        int tasksNumber = (std::rand() % teamSize) + 1;
        std::srand(std::time(0));
        for(int i = 0; i < teamSize; i++){
            if(worker[i]->isBusy())
                continue;
            if(tasksNumber == 0)
                break;
            worker[i]->setTask(workType[rand() % 3]);
            tasksNumber--;
        }
    }

    void printWorkers(){
        for(int i = 0; i < teamSize; i++){
            std::cout << worker[i]->getName() << " - task " << worker[i]->getTask() << std::endl;
        }
    }
};

class Head : public Person
{
private:
    Manager **manager;
    int teamSize;
public:
    Head(std::string name, int numManagers) : Person(name, "Head of the company"){
        teamSize = numManagers;
        manager = new Manager* [numManagers];
        for(int i = 0; i < numManagers; i++){
            std::cout << "Enter the " << i << " team size: ";
            int numWorkers;
            std::cin >> numWorkers;
            manager[i] = new Manager("Manager " + to_string(i), i, numWorkers);
        }
    }

    ~Head(){
        for(int i = 0; i < teamSize; i++)
            delete[] manager[i];
        delete[] manager;
    }

    bool isBusy(){
        bool busy = true;
        for(int i = 0; i < teamSize; i++){
            if(manager[i]->isBusy() == false){
                busy = false;
                break;
            }
        }
        return busy;
    }

    void setTask(int task){
        for(int i = 0; i < teamSize; i++){
            if(!manager[i]->isBusy())
                manager[i]->setTask(task);
        }
    }

    void printManagers(){
        std::cout << "Managers of the company:" << std::endl;
        for(int i = 0; i < teamSize; i++){
            std::cout << manager[i]->getName() << std::endl;
        }
    }

    void printCompany(){
        std::cout << "Company staff:" << std::endl;
        std::cout << getJobTitle() << " - " << getName() << std::endl;
        for(int i = 0; i < teamSize; i++){
            std::cout << "Team " << manager[i]->getTeamNumber() << std::endl;
            std::cout << manager[i]->getName() << std::endl;
            manager[i]->printWorkers();
        }
    }
};


int main()
{
    int teams;
    cout << "Enter the number of teams: ";
    cin >> teams;
    Head* head = new Head("Qwerty", teams);
    head->printCompany();
    while (!head->isBusy()) {
        int task;
        cout << "Enter the task: ";
        cin >> task;
        head->setTask(task);
        head->printCompany();
    }
    delete head;
    return 0;
}
