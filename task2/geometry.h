#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <string>
#include <cmath>

struct Coordinate{
    int x;
    int y;
};

class Figure
{
private:
    std::string color;
    Coordinate center;

public:
    Figure(int x = 0, int y = 0, std::string color = "black"){
        center.x = x;
        center.y = y;
        this->color = color;
    }

    Coordinate getCenter(){
        return center;
    }

    std::string getColor(){
        return color;
    }
};

class Circle : public Figure
{
private:
    int radius;

public:
    Circle(int radius = 1){
        this->radius = radius;
    }

    Circle(int radius = 1, int x = 0, int y = 0, std::string color = "black") : Figure(x, y, color){
        this->radius = radius;
    }

    double Area(){
        return M_PI * radius * radius;
    }

    double circumscribedRectangleArea(){
        return 4 * radius * radius;
    }
};

class Square : public Figure
{
private:
    int sideLength;
public:
    Square(int sideLength) : sideLength(1){
        this->sideLength = sideLength;
    }

    Square(int sideLength, int x, int y, std::string color) : Figure(x, y, color), sideLength(1){
        this->sideLength = sideLength;
    }

    double Area(){
        return sideLength * sideLength;
    }

    double circumscribedRectangleArea(){
        return Area();
    }
};

class Triangle : public Figure
{
private:
    int sideLength;
public:
    Triangle(int sideLength = 1){
        this->sideLength = sideLength;
    }

    Triangle(int sideLength, int x, int y, std::string color) : Figure(x, y, color), sideLength(1){
        this->sideLength = sideLength;
    }

    double Area(){
        return sideLength * sideLength * sin(M_PI / 3) / 2.;
    }

    double circumscribedRectangleArea(){
        return 2. * Area();
    }
};

class Rectangle : public Figure
{
private:
    int firstSideLength;
    int secondSideLength;
public:
    Rectangle(int firstSideLength = 1, int secondSideLength = 1){
        this->firstSideLength = firstSideLength;
        this->secondSideLength = secondSideLength;
    }

    Rectangle(int firstSideLength, int secondSideLength, int x, int y, std::string color) :
        Figure(x, y, color), firstSideLength(1), secondSideLength(1)
    {
        this->firstSideLength = firstSideLength;
        this->secondSideLength = secondSideLength;
    }

    double Area(){
        return firstSideLength * secondSideLength;
    }

    double circumscribedRectangleArea(){
        return Area();
    }
};

#endif // GEOMETRY_H
