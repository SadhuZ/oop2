#include <iostream>
#include "geometry.h"

using namespace std;




int main()
{
    string command;
    int x, y;
    string color;
    while (1) {
        cout << "Command: ";
        cin >> command;
        if(command == "circle"){
            cout << "Enter the color of the circle: ";
            cin >> color;
            cout << "Enter the coordinates of the circle. " << endl;
            cout << "X: ";
            cin >> x;
            cout << "Y: ";
            cin >> y;
            cout << "Enter the radius of the circle: ";
            int radius;
            cin >> radius;
            Circle* circle = new Circle(radius, x, y, color);
            cout << "Area = " << circle->Area() << endl;
            cout << "Circumscribed rectangle area = " << circle->circumscribedRectangleArea() << endl;
            cout << "Color = " << circle->getColor() << endl;
            delete circle;
        }
        else if(command == "square"){
            cout << "Enter the color of the square: ";
            cin >> color;
            cout << "Enter the coordinates of the square. " << endl;
            cout << "X: ";
            cin >> x;
            cout << "Y: ";
            cin >> y;
            cout << "Enter the side length of the square: ";
            int sideLength;
            cin >> sideLength;
            Square* square = new Square(sideLength, x, y, color);
            cout << "Area = " << square->Area() << endl;
            cout << "Circumscribed rectangle area = " << square->circumscribedRectangleArea() << endl;
            cout << "Color = " << square->getColor() << endl;
            delete square;
        }
        else if(command == "triangle"){
            cout << "Enter the color of the triangle: ";
            cin >> color;
            cout << "Enter the coordinates of the triangle. " << endl;
            cout << "X: ";
            cin >> x;
            cout << "Y: ";
            cin >> y;
            cout << "Enter the side length of the triangle: ";
            int sideLength;
            cin >> sideLength;
            Triangle* triangle = new Triangle(sideLength, x, y, color);
            cout << "Area = " << triangle->Area() << endl;
            cout << "Circumscribed rectangle area = " << triangle->circumscribedRectangleArea() << endl;
            cout << "Color = " << triangle->getColor() << endl;
            delete triangle;
        }
        else if(command == "rectangle"){
            cout << "Enter the color of the rectangle: ";
            cin >> color;
            cout << "Enter the coordinates of the rectangle. " << endl;
            cout << "X: ";
            cin >> x;
            cout << "Y: ";
            cin >> y;
            cout << "Enter the side 1 length of the rectangle: ";
            int sideLength1;
            cin >> sideLength1;
            cout << "Enter the side 2 length of the rectangle: ";
            int sideLength2;
            cin >> sideLength2;
            Rectangle* rectangle = new Rectangle(sideLength1, sideLength2, x, y, color);
            cout << "Area = " << rectangle->Area() << endl;
            cout << "Circumscribed rectangle area = " << rectangle->circumscribedRectangleArea() << endl;
            cout << "Color = " << rectangle->getColor() << endl;
            delete rectangle;
        }
        else if(command == "close"){
            break;
        }
        else{
            cout << "Wrong command!" << endl;
        }
    }
    return 0;
}
