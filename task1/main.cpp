#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class Branches{
private:
    int level;
    int lowestLevel;
    int size;
    Branches** branches;
    Branches* root = nullptr;
    string name;

public:

    Branches(int levels = 2){
        static bool trunk = true;
        level = levels;
        cout << "Level = " << level << endl;

        if(trunk){
            lowestLevel = levels;
            trunk = false;
        }
        else{
            string str;
            cout << "Enter elf's name:";
            cin >> str;
            if(str != "None")
                name = str;
            else
                name = "";
        }

        if(level > 0){
            double max = 2;
            if(level == 1)
                max = 4;
            srand(time(nullptr));
            size = 1 + max * rand() / RAND_MAX;

            branches = new Branches* [size];
            for(int i = 0; i < size; i++){
                branches[i] = new Branches(level - 1);
                branches[i]->root = this;
            }
        }
    }

    Branches* find(string searchingName){
        if(name == searchingName){
            return this;
        }
        if(level >= 0){
            Branches* subTree;
            for(int i = 0; i < size; i++){
                subTree = branches[i]->find(searchingName);
                if(subTree != nullptr){
                     if(subTree->root->level != lowestLevel){
                         return subTree->root;
                     }
                     else{
                         return subTree;
                     }
                }
            }
        }
        return nullptr;
    }

    string getName(){
        return name;
    }

    int countNeighbours(int counter = 0){
        if(name != "")
            counter++;
        if(level >= 0){
            for(int i = 0; i < size; i++){
                counter = branches[i]->countNeighbours(counter);
            }
        }
        return counter;
    }
};

int main()
{
    Branches* tree = new Branches();
    string name;
    cout << "Enter the searching name: " << endl;
    cin >> name;
    cout << "Number of neighbours " << tree->find(name)->countNeighbours() << endl;
    delete tree;
    return 0;
}
